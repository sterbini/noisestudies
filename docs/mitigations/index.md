Several mitigations are already in place, others may need to be improved and/or studied and developed.

### **Dipole** perturbation
- ADT is expected to suppress dipole excitation (f>3 kHz, within the limits of the its own noise and of the beam tune spread)
    - The low frequency cut off could be reduced, but this requires dedicated study and substantial hardware modification.
- CO feedback will address only slow drift (f<0.2 Hz)
- In the region 0.2 Hz < f < 3 kHz, where main triplets/BS/'10Hz’ modes lies, 
    - optimized mechanical design of the new triplets
    - improved cleaning efficiency of halo (HEL)
- At the moment, intra-bunch motion (CC amplitude noise and CC RF curvature, f>40 MHz, large efforts to reduce LLRF noise) has no safety-net.  Encouraging simulation results of CC beam-based feedback (but not in the baseline) 

### **Quadrupole** perturbation (not critical effect expected)
- no active way to address the tune modulation (tune feedback is too low in BW).
- Increase the $f_{clock}$ of the triplets switching-mode PC if possible.

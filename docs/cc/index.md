The CC cavities are one of the key ingredient for HL-LHC. Since the introduction of the concept for operation in hadron machines has been looked at one possible source of noise and emittance blow-up [1].

The density power spectrum of the amplitude and phase noise (LLRF driven) is expected to extend from DC up to 100 kHz (to compare with $f_{rev}\approx 11.245$ kHz).
Amplitude noise generate intra-bunch motion that cannot damped by the present ADT. 
Phase noise can, in principle, be mitigated by the ADT but ADT effectiveness limited by the rms tune spread of the beam (due to by HOBB + amplitude + chromatic detuning) and by the RF curvature.

Amplitude noise seems is expected to be the limiting factor: 3.7 %/h emittance growth if the challenging but possible improvement of the LLRF are successfully implemented [4].
This contribution has been included in the HL-LHC luminosity computation. Luminosity loss from the absence of CCs goes from 9 % to 7 % (nominal) and
14.7 % to 12.4 % (ultimate) [5].



A rich experimental campaign has been performed in 2018 in SPS to investigate several aspects of these novel device (first time the CC were tested in hadron machines), see [2] and [3].
In particular a test with coast beam at 270 GeV was performed to benchmark the analytical and measured emittance growth in presence of amplitude and phase noise. The first measurements of emittance growth show reasonable agreement with expectations and reaffirm the need for significantly lower noise levels for HL-LHC [4].


### Publications

- [1] [Transverse emittance growth due to rf noise in the high-luminosity LHC crab cavities](https://journals.aps.org/prab/abstract/10.1103/PhysRevSTAB.18.101001){target=_blank}, P. Baudrenghien and T. Mastoridis, Phys. Rev. ST Accel. Beams 18, 101001 (2015)

- [Assessment of the performance of High-Luminosity LHC operational scenarios: integrated luminosity and effective pile-up density](https://doi.org/10.1139/cjp-2018-0291){target=_blank}, L. Medina et al., Can. J. Phys. 97 498 (2019)

- [Emittance growth from luminosity burn-off in future hadron colliders](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.23.031002){target=_blank},
R. Tomás et al., Phys. Rev. Accel. Beams 23 031002 (2020)

- [Modeling of the emittance growth due to decoherence in collision at the Large Hadron Collider](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.23.021002){target=_blank}, X. Buffat et al., Phys. Rev. Accel. Beams 23, 021002 (2020)

### HL-WP2 Meetings

- [152nd](https://indico.cern.ch/event/826475/){target=_blank}, Dedicated to Crab Cavities (not specifically on CC noise)

- [131st](https://indico.cern.ch/event/752409){target=_blank}, Transverse emittance growth observed during last SPS CC MD with expectations, L. R. Carver, P. Baudrenghien

- [121th](https://indico.cern.ch/event/726043){target=_blank}, Transverse offset and tolerance studies for HL-LHC crab cavities, E. Yamakawa 

- [116th](https://indico.cern.ch/event/706913){target=_blank}, Tolerances to beam position offsets at the Crab Cavities, S. Antipov 

- [101st](https://indico.cern.ch/event/659970){target=_blank}, Noise mitigation by means of CC phase and amplitude feedback, T. Mastoridis 

- [5] [99th](https://indico.cern.ch/event/655317){target=_blank}, Follow up on the impact of crab cavities noise on luminosity, L. E. Medina Medrano 

- [4]: [96th](https://indico.cern.ch/event/645814){target=_blank}, Expected performances of the CC in terms of voltage and (phase) noise, P. Baudrenghien

- [96th](https://indico.cern.ch/event/645814){target=_blank}, CC operational aspects (counter-phasing when not in collision, effective voltage, injection error, transverse and longitudinal tolerances), R. Calaga

- [96th](https://indico.cern.ch/event/645814){target=_blank}, Review of the tolerances to CC phase noise (emittance blowup, ...), X. Buffat

- [96th](https://indico.cern.ch/event/645814){target=_blank}, Impact on luminosity of longitudinal oscillations, L. E. Medina Medrano

- [25th](https://indico.cern.ch/event/307357/){target=_blank}, Crab cavity operational aspects, P. Baudrenghien

### HL-TCC Meetings

- [52nd](https://indico.cern.ch/event/731926/){target=_blank}, CC test program in the SPS, R. Calaga

- [38th](https://indico.cern.ch/event/669347/){target=_blank}, SPS crab-cavities - simulations and possible test program in view of reduced voltage, F. Antoniou

### Other meetings

- [2]: [Crab Cavities: SPS MDs, Design Advancement, Plan for Production](https://indico.cern.ch/event/742082/contributions/3072164/){target=_blank}, R. Calaga, 8th HL-LHC Collaboration Meeting, CERN (2018)

- [3]: [LLRF lessons learned from SPS tests, observed emittance growth & improvements for HL-LHC](https://indico.cern.ch/event/742082/contributions/3084929/){target=_blank}, P. Baudrenghien, 8th HL-LHC Collaboration Meeting, CERN (2018)

The LHC ADT system [1] provides a maximum kick strength of 2 $\mu$rad at 450 GeV in the  3 kHz - 1 MHz BW (at 10 MHz only 10% strength is left). It can be operated in "extended BW" with a BW up to ~20 MHz.

It has 4 pick-ups per planes per beams ($\beta$-function between 100-180 m depending on the PU/plane/beams) [1]. 

From [2], the typical noise RMS is 1-2 $\mu m$ at (8-14 $\mu$m$_{\rm pk-pk}$)
The SNR is expected to be improved by more than a factor 3 after LS2.

From [3], with an operational gain of 0.04, corresponding to a damping time of 50 turns, the contribution of the noise to the emittance growth rate in LHC is estimated around 2%/h.

The details of the gain along the cycle are reported in [4].

### Publications

- [Progress In Transverse Feedbacks and Related Diagnostics for Hadron Machines](https://cds.cern.ch/record/1595483){target=_blank}, W. Hofle, IPAC2013, Shanghai, China (2013)

- [LHC transverse feedback system: First results of commissioning](https://inspirehep.net/literature/807231){target=_blank}, E.V. Gorbachev et al., RuPAC 2008, (2008)

- [Beam dynamics in synchrotrons with digital wideband transverse feedback systems](https://link.springer.com/article/10.1134/S1063779614020075){target=_blank}, V. M. Zhabitsky,  Physics of Particles and Nuclei, 45 (2014)

- [3]: [Impact of the ADT on the beam quality with high brightness beams in collision (MD2155)](https://cds.cern.ch/record/2304603){target=_blank}, X. Buffat et al., CERN, 2018

- [Modeling of the emittance growth due to decoherence in collision at the Large Hadron Collider](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.23.021002), X. Buffat et al., Phys. Rev. Accel. Beams 23, 021002

### HL-WP2 Meetings

-  [118th](https://indico.cern.ch/event/718322){target=_blank}, Review of ADT impact on emittance growth, X. Buffat

- [2]: [118th](https://indico.cern.ch/event/718322){target=_blank}, Plans for improved ADT pick up resolution, D. Valuch

- [1]: [75th](https://indico.cern.ch/event/544550){target=_blank}, Expected performance of the HL-LHC transverse feedback, W. Hofle

- [75th](https://indico.cern.ch/event/544550){target=_blank}, Do we need a Wide-Band Transverse feedback in the LHC/HL-LHC?, K. Shing Bruce Li

### Other meetings

- [4]: [ADT upgrades for HL-LHC](https://indico.cern.ch/event/742082/contributions/3084843/){target=_blank}, D. Valuch et al., 8th HL-LHC Collaboration Meeting, 16th October 2018 

- [ADT re-commissioning](https://indico.cern.ch/event/875602/){target=_blank}, D. Valuch, 116th LBOC, 10th March 2020


Seismic noise and thermal drift have been a subject of investigation since the first synchrotron and collider. 

Clear effect on induced dump was already observed during Run 2 (see the 10 Hz events [5,6]) and (mild) effects on the luminosity during the HL-LHC CE excavation works. 

Ground motion (GM) might become a relevant source of luminosity loss in HL-LHC [1].

Specific studies conducted in view of tunnel excavation works in preparation of
HL-LHC and in view of the [Geothermie 2020](https://www.geothermies.ch/) project.

Dipolar noise categories (produced from quadrupole motion coupled with ground motion):

- f <  $\approx 1$ Hz: slow orbit drifts; reduction of available orbit corrector strength; frequent machine re-alignments;

- $\approx 1$ < f < a few 100 Hz: closed orbit jitter; beam
intensity (induced dump on BLM) and luminosity losses;

- f > a few 100 Hz: emittance growth, increase in tail’s
population and  beam lifetime reduction.

Both for the orbit drift at the IPs and at the primary collimator (TCP), the effect of GM dominated by the contributions of the triplets in IP1 and IP5 (also for HL-LHC). The HL-LHC will be in general a factor 2 more sensitive to ground motion than LHC.

In absence of strong local ground motion sources the ground motion correlation drops to zero. 

Triplet expected vertical motion (magnetic axis) below 0.04 $\mu$m (for f>3 Hz) and a consequent expected luminosity losses <0.1%.

During CE work magnetic axis was expected to be below 1 $\mu$m and mainly vertical.
The present triplet quadrupoles have a strong vertical mode around 21 Hz.
In the most critical event CE drive "only" few % of luminosity loss (mainly CMS) and the highest loss spike reached only about 5% of the beam abort threshold[4]. No premature beam dumps were induced.

Preliminary HL-LHC triplet transfer function estimates suggest to be marginally better than the present triplet [4].
From studies on interplay between seismic noise and head-on beam-beam [2] no effect on emittance is expected. Direct MD studies are not yet conclusive on this aspect [3].

Possible remote alignment for slow drift, review cold mass support, and upgrade of the present orbit feedback, passive mitigations can be envisaged by improving the cleaning efficiency (using crystal collimation, using TCLDs and (for HL) hollow electron lens).


### Publications

- [Vibrational Analysis Of Tevatron Quadrupoles](https://www.slac.stanford.edu/econf/C951114/papers/013.PDF), Craig D. Moore, Fermilab, Bafavia, Illinois, USA

- [Ground Motion Measurements for FERMILAB Future Collider Projects](https://indico.cern.ch/event/1126689/){target=_blank},  V. Shiltsev et al., PAC99, New York (1999)

- [4]: [Effect of Ground Motion Introduced by HL-LHC CE Work On LHC Beam Operation](http://accelconf.web.cern.ch/ipac2019/papers/thprb116.pdf){target=_blank}, M. Schaumann et al., IPAC2019, Melbourne, Australia (2019)

- [4]: [Effect of Ground Motion Introduced by HL-LHC CE Work On LHC Beam Operation](http://accelconf.web.cern.ch/ipac2019/papers/thprb116.pdf){target=_blank}, M. Schaumann et al., IPAC2019, Melbourne, Australia (2019)
- [5]: [Special Losses During LHC Run2](https://indico.cern.ch/event/751857/contributions/3259406/attachments/1849763/3036272/EVIAN2019_DM.pdf){target=_blank}, D. Mirarchi et al., 9th LHC Operation Evian Workshop, Evian (2019)

- [1]: [Estimated Impact of Groud Motions on the HL-LHC beam orbit](http://accelconf.web.cern.ch/ipac2018/papers/thpaf040.pdf){target=_blank}, D. Gamba et al., IPAC2018, Vancouver, (2018)

- [3]: [MD1271: Effect of low frequency noise on the evolution of the emittance and halo population](https://cds.cern.ch/record/2304735){target=_blank}, M. Fitterer et al., CERN-ACC-NOTE-2018-0006 (2018)

- [2]: [Emittance Growth for the LHC Beams Due to Head-On Beam-Beam Interaction and Ground Motion](https://inspirehep.net/literature/526418){target=_blank},  M.P. Zorzano et al., FERMILAB-TM-2106 (2000)

- [Ground vibration measurements for Fermilab future collider projects](https://journals.aps.org/prab/abstract/10.1103/PhysRevSTAB.1.031001){target=_blank},
B. Baklakov et al., Phys. Rev. ST Accel. Beams 1, 031001 (1998)

- [Ground motion in LEP and LHC](https://cds.cern.ch/record/274506){target=_blank}, L. Vos, LHC Note 299, CERN (1994)

- [The Effect of Ground Motion on the LHC and HL-LHC Beam Orbit](https://cds.cern.ch/record/2847701?ln=en), M. Schaumann et al., CERN-ACC-NOTE-2022-0076


### HL-WP2 Meetings
- [169th](https://indico.cern.ch/event/886220/){target=_blank}, Operational experience and requirements from the orbit feedback implementation,  J. Wenninger

- [164th](https://indico.cern.ch/event/863720/){target=_blank}, Update on orbit stability in IP2 and IP8, J. D. Andersson

- [156th](https://indico.cern.ch/event/841437/){target=_blank}, On finding and maintaining collision in HL-LHC, J. D. Andersson, D. Gamba

- [132th](https://indico.cern.ch/event/760608){target=_blank}, Effect of civil engineering work on LHC and implications for HL-LHC, D. Gamba

- [119th](https://indico.cern.ch/event/722413){target=_blank}, Status of vibration studies, D. Gamba

- [119th](https://indico.cern.ch/event/722413){target=_blank}, Status of the cryostat for the HL-LHC insertion magnets and modal analysis, D. Duarte Ramos

- [112th](https://indico.cern.ch/event/682404){target=_blank}, Discussion on remote alignment specifications, H. Mainaud Durand, M. Sosin

- [99th](https://indico.cern.ch/event/655317){target=_blank}, Follow up of vibration studies including estimations of the impact at the collimators, D. Gamba

- [41st](https://indico.cern.ch/event/371767){target=_blank}, Impact on LHC operations for civil engineering work around IR1/IR5, M. Fitterer

### HL-TCC Meetings

- [61st](https://indico.cern.ch/event/772120/){target=_blank}, Remote alignment options and solutions, P. Fessia

- [55th](https://indico.cern.ch/event/749655/){target=_blank}, Design of triplet cryostat to optimise vibration effects, D. Duarte Ramos

- [54th](https://indico.cern.ch/event/747490/){target=_blank}, Status of the advancement of the M.S. optimisation and remote alignment study, P. Fessia, S. Claudet

- [42nd](https://indico.cern.ch/event/685625/){target=_blank}, Commissioning of high precision vibration measurement in experiments, B. Di Girolamo

- [32nd](https://indico.cern.ch/event/649470/){target=_blank}, Alignment baseline- How to technical tackle the implement the required changes and achieve the proposed performance, H. Mainaud Durand 

- [17th](https://indico.cern.ch/event/570884/){target=_blank}, Background vibration measurements in Point 5, M. Guinchard

- [10th](https://indico.cern.ch/event/515633/){target=_blank}, Alignment and internal metrology for HL-LHC, H. Mainaud Durand

- [10th](https://indico.cern.ch/event/515633/){target=_blank}, Observations on LHC triplet movements in view of HL-LHC cryostat design, J. Wenninger

- [10th](https://indico.cern.ch/event/515633/){target=_blank}, Outlook on installation of instrumentation for vibration monitoring, M. Guinchard

- [3rd](https://indico.cern.ch/event/476958/){target=_blank}, Vibration tests and measurements in LHC, J. Wenninger

### Other meetings and resources

- [6]: [Updated list of 10 Hz-like dumps](https://indico.cern.ch/event/857354/){target=_blank}, J. Wenninger et al., 111th LBOC, 29th October 2019

- [LBOC meeting dedicated to the 10 Hz-like dumps](https://indico.cern.ch/event/776486/){target=_blank}, J. Wenninger et al., 109th LBOC, 27th November 2019

- [AoB: 10 Hz observations](https://indico.cern.ch/event/780656/){target=_blank},  A. Masi, LMC 12th December 2018

- [Impact of HL-LHC civil engineering work on the LHC: do we see it and what can we learn for HL-LHC](https://indico.cern.ch/event/742082/contributions/3085160/){target=_blank}, D. Gamba, M. Schaumann, 8th HL-LHC Collaboration Meeting, 18th Oct 2018

- [Triplet cryostat design and measures to reduce impact of vibrations](https://indico.cern.ch/event/742082/contributions/3085159/){target=_blank}, D. Duarte, 8th HL-LHC Collaboration Meeting, 18th Oct 2018

- [Observation on HL-LHC CE vibration on the beam](https://indico.cern.ch/event/750340){target=_blank}, M. Schaumann, 356th LMC, 15th August 2018 

- [Observation of HL-LHC CE vibrations on the beam](https://indico.cern.ch/event/747171/){target=_blank}, M. Schaumann, 104th LBOC, 7th August 2018 

- [CE induced vibration in the LHC](https://indico.cern.ch/event/390395/){target=_blank}, 28 April 2015

- [D. Gamba's selections of links](https://espace.cern.ch/HiLumi/WP2/task1/SitePages/DavideGamba.aspx){target=_blank}, website

- [Swiss Seismic Network data](http://arclink.ethz.ch/webinterface/){target=_blank}, website.

- [On 10 Hz dumps](https://indico.cern.ch/event/1005607/contributions/4221703/attachments/2184671/3691146/10Hz-V1.pdf)


!!! important 
    An effort to classify the different noise sources into common categories has been proposed. This implies a reasonable but subjective balance between synthesis and simplification. 
    
    The presented tables should be considered as an entry point for a general overview, useful to bridge the different topics: a starting point to be improved in a collaborative approach.



### Mechanism of interaction

|    Noise source    |    Mechanism of beam interaction    |    Dipolar/quadrupolar    |
|------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------|
|    **GM**:   ground   motion and thermal effect    |    Seismic noise/thermal   drift/ancillary mechanical device vibrations (mechanically coupled with the cold   masses/magnets): change of the magnetic center of quadrupole (and higher   multipoles).    |    **Dipolar**   effect considered due to quadrupolar feed-down.    |
|    **BS**:   beam   screen vibrations    |    Induced by seismic noise and/or by   turbulent He cooling flow.   At high frequency the field follow the BS therefore a vibration is equivalent   to dipolar kicks.    |    **Dipolar**   considered (BS offset or BS  radius   vibration).    |
|    **ADT**:   damper    |    Dominated by the PU   noise (Lebedev model). The kicker reacts to the noise of the PU and excites the beam.    |    **Dipolar**   (by construction).    |
|    **PC**:   power   converters     |    By construction, harmonics   of the commuting frequency of   the semiconductor device perturb the PC output and, after filtering   (inductive load, vacuum chamber, beam screen), perturb the magnetic field,   hence the beam.    |    **Dipolar**   and **quadrupolar** considered.    |
|    **CC**:   crab   cavities    |    Dominated by the LLRF   noise in   terms of amplitude and phase of the RF kick.    |    **Dipolar**   (by construction).    |
|    **FJ**:   flux   jump    |    Related to well known physics of   the  Nb3Sn technology   (and its mild interplay with the PC). Variation of the field in the magnet.    |    Much more information on the **dipolar** noise effect. Preliminary   consideration on **quadrupolar** effect   have been made.    |
|    **HEL**:   hollow electron lens    |    Interaction with the the beam core due   to non-symmetric   distribution of the electrons   with respect beam orbit (at the moment no intra-bunch effect is   expected/studied). S-shape HEL compensate most of the “edge” effects.    |    Studies concentrated on **dipolar** kick.    |
|    **UPS**:   uninterruptible power supplies    | | |

### Potential effect on the beam

|    Noise source    |    Potential effects on the beam    |    Direct observation in LHC?    |
|------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
|    **GM**:   ground   motion and thermal effect    |    Orbit effects à Instantaneous Luminosity jitter and   beam losses at TCP  (possible dumps)    |    Yes   (during   CE work in 2018 or earthquakes). *It induced   BLM dumps in LHC (10 Hz)*.    |
|    **BS**:   beam   screen vibrations    |    Orbit effects à Instantaneous Luminosity jitter and   beam losses at TCP  (possible dumps)   Higher frequency (>1 kHz) can   cause emittance   blow-up and halo repopulation.    |    No   for f>50 Hz. Specific test conducted in 2006.   New   BS will be tested (not in the STRING).    |
|    **ADT**:   damper    |    Emittance blow-up,   beam lifetime  and halo repopulation à Integral luminosity and beam losses,   latency in instabilities.    |    Yes.   Lower   noise PU expected in Run3.    |
|    **PC**:   power   converters     |    Emittance blow-up,   beam lifetime and halo repopulation à   Integral luminosity and beam losses, latency in instabilities. Tune-tracking degraded.   Tune-modulation.    |    Yes,   dipole noise (since Run1,  extensive observations in 2018).   No,   quadrupole noise (tune modulation).       |
|    **CC**:   crab   cavities    |    Emittance blow-up,   beam lifetime and halo repopulation à   Integral luminosity and beam losses, latency in instabilities.    |    No. Extensive   MD program in SPS in 2018.    |
|    **FJ**:   flux   jump    |    Orbit effects   (mainly) à   Instantaneous Luminosity jitter and beam losses at TCP  (possible dumps)    |    No. More   data expected in Run3 (11 T dipoles).    |
|    **HEL**:   hollow electron lens    |    Emittance blow-up,   beam lifetime and core diffusion à   Instantaneous Luminosity jitter and beam losses at TCP  (possible dumps)    |    No.   MD   studies in LHC during Run2.    |
|    **UPS**:   uninterruptible power supplies    | | |


### The s-dependence

|    Noise source    |    Single or multiple locations in   the lattice?    |
|------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    **GM**:   ground   motion and thermal effect    |    Distributed effect (in principle). In   practice dominated   by the triplets   (but exception for the ‘10 Hz’-event). Depending of the frequency (>3 Hz),   limited spatial correlation is expected.    |
|    **BS**:   beam   screen vibrations    |    In practice dominated   by the triplets.   If induced by the He cooling flow, no   spatial correlation expected.     |
|    **ADT**:   damper    |    Localized at   the ADT kickers.    |
|    **PC**:   power   converters     |    Distributed. Spatial correlation expected for PCs   powering a long string of magnets (difficult to compute above few tens of   Hz).    |
|    **CC**:   crab   cavities    |    Localized at   the CC location.       |
|    **FJ**:   flux   jump    |    Localized at   the Nb3Sn magnets (IR1/5 triplets and 11 T dipoles).    |
|    **HEL**:   hollow electron lens    |    Localized at   the HEL.    |
|    **UPS**:   uninterruptible power supplies    |        |

### The t-dependence

|    Noise source    |    Effects along the beam cycle    |
|------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    **GM**:   ground   motion and thermal effect    |    Despite the GM and thermal effect are   always present, effect mainly optics-driven (high value of beta-function in   the triplets): mainly at FLATTOP and for high   tele-index. To consider evolution with   Geothermie2020.    |
|    **BS**:   beam   screen vibrations    |    See GM. Mainly at FLATTOP and for high   tele-index.    |
|    **ADT**:   damper    |    During the full cycle but   dependent on   gain of the ADT settings and of the beam tune spread…(Lebedev model).     |
|    **PC**:   power   converters     |    Dipole   component: observed during the full cycle.   Quadrupole   component: larger at high tele-index.    |
|    **CC**:   crab   cavities    |    Studies focus when crabbing bump   is active (FLATTOP).    |
|    **FJ**:   flux   jump    |    Mainly during the first part of   the ramp (<3 TeV).    |
|    **HEL**:   hollow electron lens    |    Mainly when the HEL is powered in   resonant mode (most likely a cleaning of the tail   will be done after the injection and before the squeeze, tbc)    |
|    **UPS**:   uninterruptible power supplies    |        |

### Frequency spectrum

|    Noise source    |    Frequency spectrum    |
|------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    **GM**:   ground   motion and thermal effect    |    From DC to ~100 Hz,   but significant contribution expected at the resonance of the triplets (~21   Hz).    |
|    **BS**:   beam   screen vibrations    |    New BS are quite massive due to the   tungsten masks (~500 Kg/~10 m).  First   three modes resonance between 10-20 Hz.   Test of vibration induced by turbulent flow planned but minor effects are   expected.    |
|    **ADT**:   damper    |    The noise is in the ATD BW, 3   kHz – 1 MHz  (20 MHz in extended mode).    |
|    **PC**:   power   converters     |    Harmonics   of the switching frequency.   Depending   on the technology (SCR,   silicon-controlled rectifier, or SMPS, switched-mode power supply), the   switching frequency can very different (from ~50 Hz for SCR to up to 200 kHz   for the SMPS)    |
|    **CC**:   crab   cavities    |    Two very different mechanism: phase   and amplitude noise. Both originated by the noise of the   LLRF loops (from DC to 100 kHz). The first one will appear as a bbb   kick while the second is equivalent to an intra-bunch   kick (hence beyond ADT capability)    |
|    **FJ**:   flux   jump    |    Magnetic measurement show effects   mainly between 10-100 Hz    |
|    **HEL**:   hollow electron lens    |    Spectrum will depend on the   powering mode. Assuming resonant excitation, one   should expect noise from the first betatron   line (>3 kHz). Intra-bunch kick is not considered.    |
|    **UPS**:   uninterruptible power supplies    | | |

### Expected effect on the beam

|    Noise    |    Orbit effect expected    |    Emittance blow-up    |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    **GM**:   ground   motion and thermal effect    |    HL-LHC twice more sensitive than   LHC. The 10-Hz noise induced 10 dumps in Run2.  Triplet expected vertical motion   (magnetic axis) below 0.04 μm (for f>3 Hz) and a   consequent expected luminosity losses <0.1%. Monitor effect of   Geothermie2020.    |    Negligible.    |
|    **BS**:   beam   screen vibrations    |    None   additional to GM (rigid motion triplets-BS wrt   the CM).    |    Negligible.    |
|    **ADT**:   damper    |    Negligible.    |    Estimated in LHC (Lebedev model   fit, gain of 50 turns) in 2%/h emittance growth in LHC. To maintain a similar   level for HL-LHC, ADT PU noise needs be reduced by x4. Lebedev   model implemented in the LHC luminosity model: ~0.12 um/h at injection,   ~0.045 um/h in production.    |
|    **PC**:   power   converters    |    Negligible.    |    DIPOLES: Simulations show impact   on lifetime (~15% reduction).   QUADRUPOLES: Negligible.    |
|    **CC**:   crab   cavities    |    Negligible.    |    Expected   3.7%/h (amplitude   noise) and 0.94%/h (phase   noise).    |
|    **FJ**:   flux   jump    |    DIPOLE   FJ: below   BLM threshold. QUADRUPOLE   FJ: more   critical (induced dumps expected in a non negligible number of beam) but   input needed.    |    Negligible.    |
|    **HEL**:   hollow electron lens    |    Negligible.    |    MD studies report effects larger   than the ones (negligible) expected from simulations. Details depend strongly   on the resonance mode selected. The dipole kick assumed is 15 nrad   (this equivalent to 3e-6 stability of a single main bend).    |
|    **UPS**:   uninterruptible power supplies    | | |

### Assumptions and open questions

To be filled.

|    Noise    | Assumptions, limits of the approach and open questions |
|-------------|--------------------------------------------------------|
|    **GM**:   ground   motion and thermal effect    |  |
|    **BS**:   beam   screen vibrations    |  |
|    **ADT**:   damper    |  |
|    **PC**:   power   converters    |  |
|    **CC**:   crab   cavities    |  |
|    **FJ**:   flux   jump    |  |
|    **HEL**:   hollow electron lens    |  |
|    **UPS**:   uninterruptible power supplies   | |


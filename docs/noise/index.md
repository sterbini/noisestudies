In the following we collect general papers and presentations complementary to the one reported in the other sections, where we address a single and specific noise source.

As an example you can find, among other relevant papers, the description of the Lebedev noise model [1], the effect of the noise on the instability latency [2], the present implementation of the noise source in the emittance evolution model [3].

### Publications

- [Emittance Growth due to a Small Low-frequency Perturbations](https://inspirehep.net/literature/322691){target=_blank}, K. Y. Ng, AIP Conf.Proc. 255 (1991)

- [1]: [Emittance growth due to noise and its suppression with the feedback system in large hadron colliders](https://inspirehep.net/literature/1670149){target=_blank}, V. Lebedev et al., Part.Accel. 44 (1994) 

- [On the emittance growth due to noise in hadron colliders and methods of its suppression](https://inspirehep.net/literature/458200){target=_blank},  Y. I. Alexahin, Nucl. Instrum. Meth. A 391  (1996)

- [Diffusion due to Beam-Beam Interaction and Fluctuating Fields in Hadron Colliders](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.77.1051){target=_blank}, T. Sen et al., Phys. Rev. Lett. 77, 1051 (1996)

- [Emittance growth from luminosity burn-off in future hadron colliders](https://inspirehep.net/literature/1785142){target=_blank}, R. Tomás et al, Phys. Rev. Accel. Beams 23, 031002 (2020)

- [2]: [Instability Latency in the LHC](http://accelconf.web.cern.ch/ipac2019/papers/wepts044.pdf){target=_blank}, S.V. Furuseth, IPAC2019, Melbourne, Australia (2019)

- [Emittance Preservation at the LHC](https://inspirehep.net/literature/1296496){target=_blank}, M. Kuhn, Ph.D. Thesis, CERN-THESIS-2013-031 (2013) 

### HL-WP2 Meetings

- [177th](https://indico.cern.ch/event/924095/){target=_blank}, Halo measurements using collimator scans: status and plans for Run 3,  H. Garcia Morales

- [174th](https://indico.cern.ch/event/910499/){target=_blank}, Table with the expected sources of noise: characteristics, origin and mitigation measures, G. Sterbini

- [163rd](https://indico.cern.ch/event/860231/){target=_blank}, Status of studies of the impact of noise on beam stability, S. Furuseth

- [160th](https://indico.cern.ch/event/850136/){target=_blank}, Octupoles and possible source of halo and emittance blow-up, E. Maclean

- [160th](https://indico.cern.ch/event/850136/){target=_blank}, Emittance evolution, S. Papadopoulou

- [160th](https://indico.cern.ch/event/850136/){target=_blank}, Beam lifetime in collision, G. Iadarola

- [3]: [158th](https://indico.cern.ch/event/844767/){target=_blank}, Luminosity model update, S. Papadopoulou

- [134th](https://indico.cern.ch/event/760617){target=_blank}, Impact of noise on beam stability, X. Buffat

- [134th](https://indico.cern.ch/event/760617){target=_blank}, Emittance growth in the LHC and impact on HL-LHC performance, S. Papadopoulou

- [121st](https://indico.cern.ch/event/726043){target=_blank}, 
Effect of noise on instabilities, E. Métral

- [118th](https://indico.cern.ch/event/718322){target=_blank}, Review of noise sources and impact on emittance growth, Y. Papaphilippou


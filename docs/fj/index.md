Observations with magnetic measurements on the Nb$_3$Sn magnets and relative power converters [1], showed macroscopic perturbations of the magnetic field due to the so-called flux-jumps (FJ) effect. This effect is well known for the Nb$_3$Sn superconductor [2-3]. These perturbations can results in dipolar (or higher order) noise seen by the beams. Its potential impact of the beam performance is summarized in [4] and [5].  

No direct observation of the beam impact could be reported due to the novel technology: beam-based observation are expected eventually during Run3 (11 T dipoles).
Magnetic measurement of the 11 T dipoles [5] showed a b1 amplitude noise of os 0.2 and 0.6 units in the  mean and peak values, respectively. The higher order multipoles effect is neglected. The power converter (PC) sees the FJ as a sudden and small variation of the inductance of the circuit, therefore perturbing the regulation loops [1]. The rise time of the measured magnetic perturbation is of the order of 50 ms. During Run3 we expect less than FJ per fill occurring during the ramp and in particular in the 1-3 TeV region. The excited kick (in rad) will not be energy dependent.

Considering the worst combination of 4 x 11 T dipoles (synchronous FJ), the worst energy of the beam (for proton and ions), the highest amplitude of the FJ, conservative values of beam emittances, the orbit effect at the TCP and the consequent particle loss have been estimated. These are expected to be below dump threshold. The FJ expected orbit oscillations and beam losses were  compared to the ones due to the GM and observed in 2018, the losses due to the steps of the collimators jaw when moved, the transient losses when inserting the crystal collimator in the beam, or the 10 Hz orbit oscillations observed during Run3 (in this last case several BLM triggered dumps were observed). In addition, no effect for the quench detection system is expected for Run3.

To improve the understanding of the FJ and its interplay with the beam dynamics, one could record and study the voltage observed on the trim PC of the 11 dipoles (if recorded at 1 kHz) with the ADTObsBox signals.

On the other hand, during Run4, the FJ of the new Nb$_3$Sn triplets could perturb the beam. The  magnetic noise on the multipolar content of the triplet is still under investigation. Assuming a 0.2 unit on b2 (on top of which, +0.06 b2 units have to be added for the FJ-induced noise on the PC) and its dipolar feed-down effect, the present dump threshold could be exceeded in a non negligible number of ramps.

Concerning the effect of emittance blow-up of the dipolar and quadrupolar kicks related to the FJ, from our present knowledge, no significant impact of the emittance is expected both for Run3 and HL-LHC [4]. 

A possible mitigation of the losses could be achieved by improving the  halo cleaning efficiency (e.g., hollow electron lens or, mainly for ions, crystal collimation).

### Publications

- [1]: [Impact of flux jumps on high-precision powering of Nb$_3$Sn superconducting magnets](https://inspirehep.net/literature/1745030){target=_blank}, M. Martino et al., IPAC2019, Melbourne, Australia (2019)

- [2]: [Magnetic instabilities in Nb3Sn strands and cables](https://lss.fnal.gov/archive/2004/conf/fermilab-conf-04-253-td.pdf){target=_blank}, V.V. Kashikhin, A.V. Zlobin, IEEE Trans. Appl. Supercond. 15 (2005)

- [3]: [Self-field effects in magneto-thermal instabilities for Nb$_3$Sn strands](https://inspirehep.net/literature/792277){target=_blank}, B. Bordini et al., IEEE Trans. Appl. Supercond. 18 (2008)

- [4]: [Impact of flux jumps in future colliders](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.23.011001){target=_blank}, J. Coello de Portugal et al., Phys. Rev. Accel. Beams 23, 011001 (2020)




### HL-WP2 Meetings

- [171st](https://indico.cern.ch/event/897816/){target=_blank}, Follow-up on the flux jumps for ions, Davide Gamba

- [168th](https://indico.cern.ch/event/886762/){target=_blank}, Update on impact of flux jumps in 11T dipoles, D. Gamba

- [167th](https://indico.cern.ch/event/878274/){target=_blank}, Revisiting flux jumps impact on orbit, D. Gamba

- [167th](https://indico.cern.ch/event/878274/){target=_blank}, Accuracy of transfer function measurements and field errors, L. Fiscarelli

- [167th](https://indico.cern.ch/event/878274/){target=_blank}, Measurement of (no) flux jumps during K-modulation, M. Martino

- [150th](https://indico.cern.ch/event/823530/){target=_blank}, Impact of flux jumps on orbit stability, D. Gamba

- [147th](https://indico.cern.ch/event/813823/){target=_blank}, Flux jumps impact on emittance growth, J. Coello De Portugal

- [144th](https://indico.cern.ch/event/803396/){target=_blank}, Progress on: Impact of Flux Jumps of Nb$_3$Sn Magnets on Power Converter Performance, M. Martino

- [144th](https://indico.cern.ch/event/803396/){target=_blank}, Measurements and analysis of flux jumps, L. Fiscarelli

- [133rd](https://indico.cern.ch/event/760612){target=_blank}, Effects of flux jumps on emittance blow-up, J. Coello De Portugal

- [115th](https://indico.cern.ch/event/706991){target=_blank}, Tracking and tolerance to flux jumps for triplets and 11T dipoles, J. Maria Coello De Portugal - Martinez Vazquez

- [92nd](https://indico.cern.ch/event/632391){target=_blank}, Flux Jumps Update - time scale and amplitude of the fluctuation, E. Todesco and S. Izquierdo

### HL-TCC Meetings

- [5]: [99th](https://indico.cern.ch/event/907316/){target=_blank}, Update on WP2 flux jumps studies, D. Gamba

- [78th](https://indico.cern.ch/event/829607/){target=_blank}, 11T – impact of flux jumps on WP2, WP6 and WP7, D. Schoerling


### Other meetings and resources

- [Magnetic measurements of new 11T dipoles](https://indico.cern.ch/event/875607/){target=_blank}, L. Fiscarelli, 118th LBOC, 5th May 2020

- [Expected impact of 11T dipole on beam operation and possible implementation](https://indico.cern.ch/event/875607/){target=_blank}, M. Solfaroli, 118th LBOC, 5th May 2020

- [11T: Expected impact of flux jumps at the start of Run3 and beyond](https://indico.cern.ch/event/875607/){target=_blank}, D. Gamba, 118th LBOC, 5th May 2020
    - Updated numbers wrt [99th TCC](https://indico.cern.ch/event/907316/){target=_blank}.
    - Estimated probability of a FJ-induced dump to be once every ~200 ramps.
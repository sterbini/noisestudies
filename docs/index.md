Noise sources in circular colliders can be detrimental for the beam performance, in particular in hadron machine when natural damping phenomena have longer time constants than the typical fill duration. Noise, in fact, can induce unwanted coherent and incoherent effects, such as closed orbit shift/oscillations with consequent beam losses, emittance blow-up and high amplitude diffusion with consequent tails re-populations, loss of Landau damping with consequent need for higher Laundau octupoles current...The ensemble and interplay of all these effects can produce variations in the instantaneous luminosity, premature beam dump due to BLM losses or orbit stability, additional emittance blow-up rate or beam current loss and, for the most severe cases, even quench of the superconducting devices. Finally, these effects impact the luminosity performance of the collider 

- by shortening the luminosity leveling/production period with respect to the ideal one, and 

- by increasing the average turn-around-time of the machine.

The HL-LHC foreseen important hardware changes adopting novel technologies (Nb$_3$Sn triplets and 11 T dipoles, crab cavities, hollow electron lens, new power-converters and beam screen for the triplets...) and unprecedented large beta-functions in the final focusing quadrupoles and in the ATS arcs. 

A long-term scrutiny for the LHC and HL-LHC noise sources and investigation on possible solutions/mitigations has been launched by the [HL-LHC Work Package 2](https://hilumilhc.web.cern.ch/wp/wp2-accelerator-physics-performance){target=_blank} (WP2).

In this web-pages, we summarise the main outcome by

1. collecting information about the known sources of noise and their present understanding,
2. defining a common framework for the different typologies of noise,
3. normalizing the information following common paradigm (e.g., a summary table).

The goal is to serve, mainly, the HL-LHC WP2 community as suggested in the [WP2 noise action list](https://espace.cern.ch/HiLumi/WP2/Wiki/Team%20Discussions.aspx){target=_blank}. In particular following the specific actions:

- [noise and emittance blow-up](https://espace.cern.ch/HiLumi/WP2/_layouts/15/WopiFrame.aspx?sourcedoc=/HiLumi/WP2/Lists/Tasks/Attachments/236/Noise_andEmittanceBlow-up_27092019_uptodate.docx&action=default){target=_blank}

- [noise and flat-optics](https://espace.cern.ch/HiLumi/WP2/_layouts/15/WopiFrame.aspx?sourcedoc=/HiLumi/WP2/Lists/Tasks/Attachments/235/BB-WS_Flat_Optics_25092019_uptpdate.docx&action=default){target=_blank}.

After this short introduction, we analyse different families of source of noise. After a (limited) collections of the resources on the noise impact on the beam,  [Effects of the noise](noise/index.md), we summarize the noise characteristic of the following sources:

- [Transverse Damper](adt/index.md)
- [Beam Screen Vibrations](bsv/index.md)
- [Crab Cavities](cc/index.md)
- [Flux Jumps](fj/index.md)
- [Ground Motion and Thermal Drifts](gm/index.md)
- [Hollow Electron Lens](hel/index.md)
- [Power Converters](pc/index.md)
- [Uninterruptible Power supplies](ups/index.md)

Clearly, there are other relevant phenomena potentially affecting the nominal luminosity performance of the collider, such as the ones related to

- static magnetic field quality and/or fringe field,
- electron cloud,
- beam-beam effect in nominal and/or PACMAN bunches,
- emittance blow-up due to luminosity burn-off or intra-beam scattering,
- injection oscillations,
- transverse and longitudinal instabilities,
- long term (> fill duration) reproducibility of the power supplies,
- high order mode of the RF devices (namely the crab-cavities),
- aperture restriction due to mis-alignment, mechanical obstructions (ULO) or limited closure of crossing/dispersion/crabbing bumps,
- contamination of the BS/vacuum chamber (16L2-type),
- UFO events,
- quench triggered by spurious EM signals,
- hardware failure,...


These effects are beyond the scope of the present summary. 

Additional resources and complementary materials related to the HL-WP2 activity can be found [here](https://espace.cern.ch/HiLumi/WP2/Wiki/References.aspx){target=_blank}.
In particular, the HL-LHC parameters can be retrieved [here](https://espace.cern.ch/HiLumi/WP2/Wiki/HL-LHC%20Parameters.aspx){target=_blank}.

Another valuable source of information is the web site maintained by D. Gamba ([link](https://espace.cern.ch/HiLumi/WP2/task2/SitePages/DavideGamba.aspx){target=_blank}).

!!! info "Share your knowledge with us."

The platform is based on [mkdocs](https://gitlab.cern.ch/authoring/documentation/mkdocs){target=_blank} + [gitlab](https://gitlab.cern.ch/){target=_blank}. 
To edit this site just click the pencil symbol at the top-right.
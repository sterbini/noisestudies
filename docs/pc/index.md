Tolerance on the reproducibility and ripple of the HL-LHC power converters (PC) are defined in [1].

PC noise can result in a time-variation of the magnetic fields of the aperture (via several filtering effects of the circuit impedance and screening effects [2]).
The effect of the dipolar and quadrupolar pertubation were considered in simulation for HL-LHC taking into account the observed behavior of the.

The noise spectrum is not white but consists of tones multiple of the commuting frequency of the semiconductor device of the PC (e.g., the clock frequency of the switched-mode power supplies (SMPS) or the mains frequency for Silicon Controlled Rectifier,SCR). 

During 2018 an intense experimental and simulation campaign took place [3] and [4]. From the beam spectrum it is not visible any tune/quadrupole modulation, while is apparent a position/dipole modulation (mainly in H-plane, more in B1 than in B2). 50 Hz harmonics appears up to 8 kHz, i.e., $(1-Q)f_{rev}$, and they are related to the interplay between the 8 arc dipoles power supplies and the ADT. First investigations from the impedance teams, seems to exclude a significant role of the coupled-bunch instability in the picture [5].

It is important to note that due to their nature (same PC powering the string of 154 arc dipoles), we cannot assume in principle uncorrelated noise between the dipoles belonging to the same string. Due to the common source of the perturbation (50 Hz of the mains), one can assume that there is also a correlation between the 8 main dipoles PC. 

For the reason mentioned above, a direct a application of the Lebedev model is not possible for this problem and tracking simulations were performed.
From [3], based on a realistic noise spectrum and a simplified lumped transfer function of the noise, simulations indicate that these excitations increase the diffusion of the particles. From a tracking that corresponds to 90 seconds in operation, the excitation of additional resonances eventually led to proton losses, especially due to the high-frequency cluster. Based on these results, it is concluded that the 50 Hz harmonics had an impact on the beam performance during the LHC operation. In addition, due to the discrepancy of the noise spectrum in Beam 1 and 2, the aforementioned simulations illustrated a clear discrepancy in the intensity evolution of the two beams. An important lifetime asymmetry between the two beams has been observed since the beginning of run 2 and it is the first time that tracking simulations show that noise can contribute to this effect.

From [4],  a comparison with the new triplet power supply specifications, without considering the shielding effects of the magnets that lead to a further attenuation of the ripples, yielded that the maximum expected power supply noise level is several orders of magnitude lower than the DA reduction threshold.

Data on the measurements on the LHC PC can be found in /eos/project/a/abpdata/lhc/rawdata/power_converter.

### Publications
- [Beam Dynamics Requirements for the Powering Scheme of the HL-LHC Triplet](https://inspirehep.net/literature/1417776){target=_blank}, M. Fitterer et al., IPAC 2015, Richmond, Virginia, USA (2015)

- [3]: [Impact of the 50 Hz harmonics on the beam evolution of the Large Hadron Collider](https://cds.cern.ch/record/2713702){target=_blank}, S. Kostoglou et. al.,  (submitted for publication)

- [4]: [Tune modulation effects in the High Luminosity Large Hadron Collider](https://cds.cern.ch/record/2712053){target=_blank}, S. Kostoglou et. al.,  (submitted for publication)

- [MD4147: 50 Hz harmonics perturbation](https://cds.cern.ch/record/2703609){target=_blank}, S. Kostoglou et. al., CERN (2019)

- [MD1271: Effect of low frequency noise on the evolution of the emittance and halo population](https://cds.cern.ch/record/2304735){target=_blank}, F. Miriam et al., CERN (2018)

- [2]: [Magnetic Frequency Response of High-Luminosity Large Hadron Collider Beam Screens](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.22.013501){target=_blank}, M. Morrone et al., Phys. Rev. Accel. Beams 22, 013501 (2019)

- [1]: [Beam dynamics requirements for HL–LHC electrical circuits](https://cds.cern.ch/record/2298764?ln=en){target=_blank}, D. Gamba et al., CERN-ACC-2017-0101, CERN (2017)

- [Propagation of the 50 Hz Perturbations in the Large Hadron Collider Sector Dipoles and Equivalent Model Circuit](https://cds.cern.ch/record/2715844){target=_blank}, L. Intelisano, 	CERN-THESIS-2020-026 (2020)

- [Methods and results of modeling and transmission-line calculations of the superconducting dipole chains of CERN's LHC collider](http://cds.cern.ch/record/592974/files/lhc-project-report-497.pdf), F. Bourgeois and K. Dahlerup-Petersen, LHC Project Report 497 (2001) 

### HL-WP2 Meetings

- [174th](https://indico.cern.ch/event/910499/){target=_blank} postponed to [175th](https://indico.cern.ch/event/911170/){target=_blank}, Summary of power supply ripple observations in the LHC and impact on the HL-LHC, S. Kostoglou

- [163rd](https://indico.cern.ch/event/860231/){target=_blank}, Status of noise source studies in the LHC, expected impact for HL-LHC, Sofia Kostoglou

- [153rd](https://indico.cern.ch/event/831847/){target=_blank}, PC stability at the end of the ramp, M. Cerqueira Bastos

- [137th](https://indico.cern.ch/event/773002/){target=_blank}, Amendment to HL-LHC circuits specifications, D. Gamba

- [137th](https://indico.cern.ch/event/773002/){target=_blank}, Power converter specification, B. Cerquira Bastos

- [129th](https://indico.cern.ch/event/752397){target=_blank}, Status of the studies on 50 Hz multiple noise and implications for HL-LHC, S. Kostoglou

- [99th](https://indico.cern.ch/event/655317){target=_blank}, Impact of noise in the main dipoles - orbit, induced energy deviation and tune ripple, D. Gamba, J. Maria Coello De Portugal - Martinez Vazquez

- [88th](https://indico.cern.ch/event/623917){target=_blank}, Status Update of Power Converter Precision & Accuracy Performance, M. Martino, M. Cerqueira Bastos

- [66th](https://indico.cern.ch/event/463032){target=_blank}, Noise estimates for HL-LHC superconducting circuits, M. Cerqueira Bastos

- [63rd](https://indico.cern.ch/event/463028){target=_blank}, Power converter reproducibility specifications for HL-LHC, J. Maria Coello De Portugal - Martinez Vazquez

- [32nd](https://indico.cern.ch/event/323862){target=_blank}, Powering requirements (topology, noise levels, reproducibility, accuracy) for HL-LHC triplet, M. Fitterer

### HL-TCC Meetings

- [71st](https://indico.cern.ch/event/807209/){target=_blank}, Powering accuracy specification for all power converter types, D. Gamba

- [63rd](https://indico.cern.ch/event/779650/){target=_blank}, Summary of observations on noise for LHC and projections for HL-LHC, S. Kostoglou

### Other Meetings/Resources

- [Update on the study of 50Hz lines on the beams](https://indico.cern.ch/event/857354/){target=_blank}, S. Kostoglou, 111th LBOC meeting, 29th October 2019

- [Analysis and observation of 50 Hz harmonics](https://indico.cern.ch/event/747171/){target=_blank}, G. Sterbini et al., 104th LBOC, 7th August 2018 

- [5]: [50 Hz lines investigations](https://indico.cern.ch/event/883737/){target=_blank}, X. Buffat, 201st HSC section meeting, 10th February 2020

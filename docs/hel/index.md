The HEL aims to improve the cleaning efficiency of the collimation system by actively controlling the halo depletion speed for amplitude of betatron motion below the aperture restriction of the primary collimators.

Studies of the beam dynamics simulations showed that the most effective halo depletion can be reached in pulsed mode [1]. 

EM leakage of the HEL field can potentially affect the beam core (due to imperfection of the electron beam distribution).
This can generate kicks equivalent to RF multipoles. Therefore a trade-off between halo depletion speed and core diffusion has to be considered by conveniently chose the DC/pulsed HEL mode along the cycle.

Simulation studies and experimental measurements investigated the effect of the HEL on the beam core [2,3].

The maximum kick in the halo region of the HEL is 375 nrad.

No effect on the beam core in TEVATRON during DC mode operation [2].
In [2] only the dipolar kick is studied. The main contributor to the dipolar kick is expected to be the one driven by the profile imperfection (15 nrad at 7 TeV).

Experiment were conducted at LHC injection [3]: for 15 nrad kick one can observe losses (10-20% for 7th turn H excitation) and/or emittance blow-up (43% for 10th vertical excitation).


### Publications

- [Effect of pulsed hollow electron-lens operation on the proton beam core in LHC](https://inspirehep.net/literature/1496416){target=_blank}, 
M.Fitterer, G. Stancari, A. Valishev, FERMILAB-TM-2635-AD, FERMILAB (2016)

- [2]: [Hollow Electron Beam Collimation for HL-LHC - Effects on the Beam Core](https://inspirehep.net/literature/1622968){target=_blank}, M. Fitterer et al., IPAC 2017, Copenhagen, Denmark (2017)

- [3]: [Effect of a resonant excitation on the evolution of the beam emittance and halo population](https://cds.cern.ch/record/2305121/files/CERN-ACC-NOTE-2018-0007.pdf){target=_blank}, M. Fitterer et al., CERN (2018) 

- [Resonant and random excitations on the proton beam in the Large Hadron Collider for active halo control with pulsed hollow electron lenses](https://arxiv.org/pdf/1804.07418.pdf){target=_blank}, M. Fitterer et al., FERMILAB-PUB-18-084-AD-APC (2019)

- [Hollow Electron-Lens Assisted Collimation and Plans for the LHC](http://accelconf.web.cern.ch/hb2018/papers/tup1we02.pdf){target=_blank}, D. Mirarchi et al., HB2018, Daejeon, Korea (2018)

- [Halo removal experiments with hollow electron lens in the BNL Relativistic Heavy Ion Collider](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.23.031001){target=_blank}, X. Gu et al., Phys. Rev. Accel. Beams 23, 031001 (2020)


### HL-WP2 Meetings

- [1]: [161th](https://indico.cern.ch/event/858460/){target=_blank}, Beam dynamics simulations with hollow electron lens, D. Mirarchi

### HL-TCC Meetings

- [17th](https://indico.cern.ch/event/570884/){target=_blank}, Design considerations for an electron-lens test stand at CERN, G. Stancari

- [17th](https://indico.cern.ch/event/570884/){target=_blank}, Analysis of lifetime drops in view of e-lens review, B. M. Salvanchua Ferrando and S. Redaelli



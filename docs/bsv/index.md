In the past, vibrations in the frequency range of 600-1400 Hz were observed on the cold mass of the SSC dipole magnet [1]. In large machine, the relatively low revolution frequency implies that a betatronic frequency of few KHz.


 One can assume that the beam screen of LHC can be excited to similar frequency from the turbulent He cooling at 4-20 K (supercritical, 4 bar). Some experiments in that sense were conducted [2] but not always they could be carried out with an optimal setup. The correlation length of the turbulent He cooling is expected to be much shorter than the magnet length [1].

Clearly the ADT can cure it only in the ADT BW (> 3 kHz).

Assuming white noise spectrum and considering only the effect at the betatron frequency, one can compute the 
\begin{equation}
\Delta\epsilon_{nx}(t)= \frac{\gamma_r \beta_x }{2} \theta_{x,rms}^2 f_{rev} t,
\end{equation}
with the usual meaning on the symbols.
The previous equation can be generalized to multiple independent magnets errors or for colored spectrum of noise [1].

Recent analytical and FE studies from C. Garion et al. [3] focused on the beam screen of the new HL triplets since, also from ground motion, studies, this is expected to me the most critical location (and only in the triplet the beam screen will be replaced).

The new beam screen is very different from the one presented in [2]. The main difference is that will be tungsten loaded (a single quadrupole beam screen weights ~500 kg).

The first 3 modes of vibration in the 10-20 Hz (significantly lower of the response observe on the dipole beam screen tested in [2]).
- No enhancement of ground motion is expected. 
- BS is "magnetically locked" inside the Cold Mass.

Dedicated measurements are planned (frequency and amplitude) of turbulent flow induced vibration in cooling tubes (but not dedicated measurement during the STRING test) [4].

From the analysis of LHC spectra analysis in the range > 50 Hz no special frequencies but the 50 Hz harmonics, tunes and f$_{rev}$ tones are visible.

### Publications

- [Ground Motion Measurements for FERMILAB Future Collider Projects](https://indico.cern.ch/event/1126689/){target=_blank},  V. Shiltsev et al., PAC99, New York (1999)

- [1]: [Magnetic Field Fluctuations in SC Dipole Magnet](https://accelconf.web.cern.ch/p01/PAPERS/RPPH102.PDF){target=_blank}, V. Shiltsev et al., PAC01, Chicago (2001)

- [2]: [Field Fluctuation and Beam Screen Vibration Measurements in the LHC magnets](https://cds.cern.ch/record/1058522/files/lhc-project-report-1041.pdf){target=_blank}, R. De Maria et al., PAC07, Albuquerque (2007)

- [Beam screen issues](https://arxiv.org/abs/1108.1643){target=_blank}, E. Métral, CERN Yellow Report CERN-2011-003 (2010)

- [3]: [Natural vibration frequencies of shielded beam screens](https://edms.cern.ch/document/2031211/2.4){target=_blank}, C. Garion et al., EDMS 2031211 v.2.4 (2019)

- [Vibration test of the 10 m long LHC beam screen](https://edms.cern.ch/ui/#!master/navigator/document?D:100272937:100272937:subDocs){target=_blank}, M. Sitko, EDMS 2067643

- [Magnetic frequency response of High-Luminosity Large Hadron Collider beam screens](https://inspirehep.net/literature/1636694){target=_blank}, M. Morrone et al., Phys. Rev. Accel. Beams 22, 013501 (2019) 

- [Mechanical Behavior of the Shielded HL-LHC Beam Screen during a Magnet Quench](http://cds.cern.ch/record/2776364/files/CERN-ACC-2021-0005.pdf), M. Morrone et al., CERN-ACC-2021-0005 (2021)


### HL-WP2 Meetings

- [119th](https://indico.cern.ch/event/722413){target=_blank}, Impact of beam screen vibrations on the beam. Summary of the results obtained for the LHC and possible comparisons with beam observations, R. De Maria

### HL-TCC Meetings

- [4]: [79th](https://indico.cern.ch/event/834534/){target=_blank}, Action follow-up: WP2, WP3 and WP12 should assess whether a beam screen vibration test on a long magnet should be performed outside the STRING, D. Gamba


### Other presentations

- [Field Fluctuation in LHC magnets](https://larpdocs.fnal.gov/LARP-public/DocDB/ShowDocument?docid=174){target=_blank}, V. Shiltsev, BLNL LARP Collaboration, 26th-28th April 2006

- [Impact of beam screen vibrations on field quality and emittance growth](http://fqwg.web.cern.ch/fqwg/060502/rm060502.pdf){target=_blank}, R. De Maria et al., Field Quality Working Group, 2nd May 2006

- [Status of beam screen / cold bore / interconnect / CWT design & production](https://indico.cern.ch/event/742082/contributions/3085182/){target=_blank}, C. Garion, 8th HL-LHC Collaboration Meeting, 18th October 2018

- [Beam screen design](https://indico.cern.ch/event/725045/){target=_blank}, C. Garion, HL-LHC Inner Triplet BPMs Conceptual Design Review, 17th May 2018

- [Test of beam screen on short model](https://indico.cern.ch/event/780357/){target=_blank}, M. Morrone, 16th January 2019

- [Impact of beam screen vibrations on field quality and emittance growth](https://fqwg.web.cern.ch/fqwg/060502/rm060502.pdf)

- [Vibration test of the 10 m long LHC beam screen](https://edms.cern.ch/document/2067643/1), EDMS 2067643